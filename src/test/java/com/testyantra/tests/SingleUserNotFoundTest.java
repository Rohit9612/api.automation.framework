package com.testyantra.tests;

import com.testyantra.TestBase;
import com.testyantra.constants.EndPoints;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;

public class SingleUserNotFoundTest extends TestBase {
    @Test
    public void singleUserNotFoune (){

        given().contentType(ContentType.JSON).when().get(EndPoints.SINGLE_USER_NOT_FOUND_ENDPOINT).then().
        extract().response().prettyPrint();
        given().contentType(ContentType.JSON).when().get(EndPoints.SINGLE_USER_NOT_FOUND_ENDPOINT).then().
                statusCode(404);


    }
}
