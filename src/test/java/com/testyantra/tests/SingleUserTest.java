package com.testyantra.tests;

import com.testyantra.TestBase;
import com.testyantra.constants.EndPoints;
import io.restassured.http.ContentType;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
public class SingleUserTest extends TestBase{


    @Test
    public void testSingleUser(){

        given().contentType(ContentType.ANY.JSON).
           when().get(EndPoints.SINGLE_USER_ENDPOINT).then().body("data.first_name",equalTo("Janet"));
    }
}
