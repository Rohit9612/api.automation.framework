package com.testyantra.tests;

import com.testyantra.TestBase;
import com.testyantra.constants.EndPoints;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;

public class ListUsersTest extends TestBase{


    @Test
    public void testUsers(){
// given,when,then


        given().contentType(ContentType.JSON).when().get(EndPoints.USERS_ENDPOINT).then().statusCode(200);
    }

    @Test
    public void testUsersResponse(){

        given().contentType(ContentType.JSON).when().get(EndPoints.USERS_ENDPOINT).then()
                .extract().response().prettyPrint();
    }
}
